package main

import (
	"data"
	"file"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"net/http"
	"web"
)

/* view for town */
type TownView struct {
	Number    int    /* number of table */
	Name      string /* name of town */
}

const (
	mainHTMLFile       = "main.html"       /* main web-page */
)

var (
	userVariables   *file.UserVariables   /* structure for user variables */
	handlerDatabase *data.HandlerDatabase /* structure to handle database */
)

/* function to load main page */
func mainInit(writer http.ResponseWriter, request *http.Request) {

	var (
		townsView  []TownView   /* view of towns */
		towns     *[]data.Town /* towns of database */
		i         int          /* iterator */
	)

	/* get towns from database */
	towns = handlerDatabase.GetTowns()

	/* create and fill view for towns */
	townsView = make([]TownView, len(*towns))
	i = 0
	for i < len(*towns) {
		townsView[i].Number = (*towns)[i].Number
		townsView[i].Name = (*towns)[i].Name
		i++
	}

	/* load web-page */
	web.PageLoad(writer, mainHTMLFile, townsView)

}

/* entry point */
func main() {

	var (
		router *mux.Router /* router for pages */
		err    error       /* variable for error */
	)

	/* write initial message */
	fmt.Println("Server for information system")

	/* read configuration file with writing of message */
	fmt.Println("Read configuration file...")
	userVariables = new(file.UserVariables)
	file.ReadConfigFile(userVariables)

	/* create handler of database */
	handlerDatabase, err = data.NewHandlerDatabase(userVariables.NameDB, userVariables.DataDB)
	file.HandleErrorFile(err)

	/* create and fill router for pages with writing of message */
	fmt.Println("Launching of server...")
	router = mux.NewRouter()
	router.HandleFunc("/", mainInit).Methods("GET")

	/* launch http-server for working with writing of message */
	fmt.Println("Server is working...")
	http.Handle("/", router)
	err = http.ListenAndServe((*userVariables).IpAddress + ":" + (*userVariables).PortServer, nil)
	file.HandleErrorFile(err)

	/* close handler of database */
	fmt.Println("Server finishes working...")
	handlerDatabase.Close()

}
