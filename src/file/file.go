package file

import (
	"bufio"
	"io"
	"os"
	"strings"
	"time"
)

/* structure of user variables */
type UserVariables struct {
	NameDB     string
	DataDB     string
	IpAddress  string
	PortServer string
}

const (
	errorFile  = "error.txt"  /* file for error */
	configFile = "config.txt" /* file for configuration */
)

/* function to handle error and write message to the file */
func HandleErrorFile(err error) {

	if err != nil {

		var (
			file *os.File /* variable for file */
		)

		/* open file */
		file, _ = os.OpenFile(errorFile,
			os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)

		/* write error into file */
		_, _ = file.WriteString(time.Now().String() + " " + err.Error() + "\n")

		/* close file */
		_ = file.Close()

	}

}

/* function to read configuration file */
func ReadConfigFile(userVariables *UserVariables) {

	var (
		file       *os.File      /* variable for file */
		reader     *bufio.Reader /* reader of file */
		err        error         /* variable for error */
		line       []byte        /* byte array of line */
		row        string        /* row of file */
		substrings []string      /* substrings for parsing */
	)

	/* open file */
	file, err = os.Open(configFile)
	HandleErrorFile(err)

	/* create reader */
	reader = bufio.NewReader(file)

	for {

		/* read line and convert to string */
		line, _, err = reader.ReadLine()
		if err == io.EOF {
			break
		} else if err != nil {
			HandleErrorFile(err)
		}
		row = string(line)

		/* check if this row is comment */
		if row[0] != '#' {

			/* split row using symbol "=" */
			substrings = strings.Split(row, "=")

			/* check correctness of row */
			if len(substrings) >= 2 {

				/* trim row using symbol "space" */
				substrings[0] = strings.TrimSpace(substrings[0])
				substrings[1] = strings.TrimSpace(substrings[1])

				/* save result of handling */
				if substrings[0] == "NameDB" {
					(*userVariables).NameDB = substrings[1]
				} else if substrings[0] == "DataDB" {
					(*userVariables).DataDB = substrings[1]
				} else if substrings[0] == "Port" {
					(*userVariables).PortServer = substrings[1]
				} else if substrings[0] == "IpAddress" {
					(*userVariables).IpAddress = substrings[1]
				}

			}

		}

	}

}
