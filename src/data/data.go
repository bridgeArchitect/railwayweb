package data

import (
	"database/sql"
	"file"
)

/* constant for array */
const (
	sizeArray = 100
)

/*type "User" for database */
type Town struct {
	Id     int
	Number int
	Name   string
}

/* structure to handle database */
type HandlerDatabase struct {
	db *sql.DB
}

/* constructor for type "HandlerDatabase" */
func NewHandlerDatabase(nameDB string, dataDB string) (*HandlerDatabase, error) {

	var (
		handlerDatabase *HandlerDatabase /* handler of database */
		db              *sql.DB          /* handler of database */
		err             error            /* variable for error */
	)

	/* create object */
	handlerDatabase = new(HandlerDatabase)

	/* open database and check error */
	db, err = sql.Open(nameDB, dataDB)
	if err != nil {
		return nil, err
	}

	/* save connection and return object */
	handlerDatabase.db = db
	return handlerDatabase, nil

}

/* function to get towns from database */
func (handler *HandlerDatabase) GetTowns() *[]Town {

	var (
		rows     *sql.Rows /* rows of database */
		err      error     /* variable for error */
		towns    []Town    /* towns of system */
		curTown  Town      /* current town */
	)

	/* make query to get towns */
	rows, err = (*handler).db.Query("select * from towns")
	file.HandleErrorFile(err)

	/* create array of towns */
	towns = make([]Town, 0, sizeArray)

	/* receive towns */
	for rows.Next() {
		err = rows.Scan(&curTown.Id, &curTown.Number, &curTown.Name)
		file.HandleErrorFile(err)
		towns = append(towns, curTown)
	}

	/* return towns */
	return &towns

}

/* function to close handler of database */
func (handler *HandlerDatabase) Close() {

	var (
		err error /* variable for error */
	)

	/* close database */
	err = (*handler).db.Close()
	file.HandleErrorFile(err)

}